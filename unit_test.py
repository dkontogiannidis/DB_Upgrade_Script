import unittest
import mysql.connector as ms
import json
from upgrade import extract_version, main
from mock import patch
import os
import re


class MyTestCase(unittest.TestCase):

    def setUp(self):
        """
        Prepare the environment for running the tests. Initialize the database, create a table and insert
        dummy data to be used.
        """
        with open('db_config.json') as db_config:
            # Connect to mysql after retrieving the required information from configuration JSON
            self.config_dict = json.load(db_config)
            test_db = ms.connect(**self.config_dict)
            # Execute sql statements
            cursor = test_db.cursor()
            cursor.execute('CREATE DATABASE versiondb;')
            cursor.execute("USE versiondb;")
            cursor.execute('''
                                    CREATE TABLE versionTable(
                                      version INT NOT NULL,
                                      PRIMARY KEY (version)
                                    );                                   
                                ''')
            cursor.execute('''
                                    INSERT INTO versionTable (version)
                                    VALUES (2);
                                ''')
            # Apply changes to the database.
            test_db.commit()
            # Close connection to database
            test_db.close()

    def tearDown(self):
        """
        Delete the testing table and close the database connection.
        """
        test_db = ms.connect(**self.config_dict)
        cursor = test_db.cursor()
        # Delete the database
        cursor.execute("DROP DATABASE versiondb;")
        test_db.commit()
        test_db.close()

    def test_version_extraction_from_file(self):
        """
        A unit test case used to check if the version extraction from the upgrade files occurs correctly.
        """
        self.assertEqual(10, extract_version("010.createtable.sql")["version"])
        self.assertEqual(56, extract_version("056createtable.sql")["version"])

    def test_functional(self):
        """
        Test the whole functionality of the script.
        Mock is used here to simulate the command line arguments.
        """
        test_args = ["sql_scripts", self.config_dict["user"], self.config_dict["host"], "versiondb",
                     self.config_dict["password"]]
        with patch('sys.argv', test_args):
            main()
        test_db = ms.connect(**self.config_dict)
        cursor = test_db.cursor()
        cursor.execute("USE versiondb;")
        test_db.commit()
        cursor.execute("SELECT version FROM versionTable")
        self.assertEqual(max(map(lambda x: int((re.search('[0-9]+', x)).group(0)), os.listdir(test_args[0]))),
                         cursor.fetchall()[0][0])
        cursor.close()
        test_db.close()


if __name__ == '__main__':
    unittest.main()
