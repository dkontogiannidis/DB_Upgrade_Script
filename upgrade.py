from os import listdir, path
import re
import mysql.connector as ms
from mysql.connector import errorcode
import sys


def extract_version(filename):
    """
    :param filename: A string containing the filename
    :return: A dictionary containing the version and the name of the update script
    """
    try:
        # Use a regular expression to extract the version from the sql script
        return {'version': int((re.search('[0-9]+', filename)).group(0)), 'file': filename}
    except AttributeError:
        print "WARNING: The script {} does not match the specification.".format(filename)
        # Return a version that will not be used
        return {'version': -1, 'filename': ''}


def get_db_version(db):
    """
    :param db: The database connection
    :return: An integer indicating the version of the database.
    """
    cursor = db.cursor()
    try:
        cursor.execute('SELECT version FROM versionTable ORDER BY version DESC')
        entry = cursor.fetchall()[0][0]
        print "INFO: VERSION AT DATABASE IS {}.".format(entry)
        return entry
    finally:
        cursor.close()


# def is_db_up_to_date(version, scripts):
#     """
#     :param version: Current Version of the database
#     :param scripts: The list of the database upgrade scripts
#     :return: A boolean value indicating if the database needs to be updated.
#     """
#     return version >= scripts[-1]  # The max version from scripts


def main():
    try:
        # Connect to the Database
        db = ms.connect(user=sys.argv[1], host=sys.argv[2], database=sys.argv[3], password=sys.argv[4])
        # Add each element of the list in a dictionary containing the version and the filename
        # Sort the list to apply the version from the smaller to the bigger value
        scripts = sorted(map(extract_version, listdir('sql_scripts')), key=lambda x: x["version"])
        # Get the current version in the database
        version = get_db_version(db)

        # Keep only the scripts with a higher version than the database
        scripts = filter(lambda x: x["version"] > version, scripts)
        cursor = db.cursor()
        for upgrade_script in scripts:
            # Open the script to be applied to the database
            with open(path.join('sql_scripts', upgrade_script["file"])) as f:
                print "INFO: EXECUTING {}.".format(f.name)
                # Execute each line separately.
                for line in f.readlines():
                    cursor.execute(line)
                # Update database version
                cursor.execute("""
                                UPDATE versionTable SET version = {}
                               """.format(upgrade_script["version"]))
                # Apply changes to DB. We want it to be only one transaction so if something fails no
                # changes are applied to the database
                db.commit()
        if get_db_version(db) > version:
            print "INFO: DB UPDATED TO VERSION {}.".format(get_db_version(db))
        else:
            print "INFO: DB WAS ALREADY UP TO DATE, NO CHANGES APPLIED."
        db.close()
    except ms.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("ERROR: Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("ERROR: Database does not exist")
        else:
            print(err)
        raise


if __name__ == '__main__':
    main()
