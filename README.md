# NOTES

This script is used to automatically check the version of the database with a set of scripts in a given directory.
If the database is outdated then all the sql scripts with a grater version than the existing will be executed against
the database.

## REQUIREMENTS:
    - PLATFORM: independent
    - PYTHON: 2.7
    - DATABASE: MySQL

## TESTING THE SCRIPT:
    pip install -r requirements.txt
    (If it fails to install the mysql connector please install it manually from https://dev.mysql.com/downloads/connector/python/2.1.html)

    * For testing only the db_config.json must be altered to contain the details of a valid MySQL connection.

    You can run the unit_test.py which tests the implementation by automatically creating and destroying the
    database. It performs two tests one simple unit test to check that version extraction from the sql script
    is performed correctly and another one which tests the functionality of the db_upgrade.py by simulating
    the upgrading routine.

    Note that the unit_test need not to be provided with command line arguments as
    they are provided to the test through mocking them.


## HOW TO RUN THE SCRIPT:
    pip install -r requirements.txt
    (If it fails to install the mysql connector please install it manually from https://dev.mysql.com/downloads/connector/python/2.1.html)

    python upgrade.py <directory with .sql scripts> <username for the DB> <DB host> <DB name> <DB password>

## RECOMMENDATION:
    This script can be automatically called by a scheduler for example CRON JOBS in Linux Task Scheduler in Windows
    or a platform independent solution provided by Python, AP Scheduler.




